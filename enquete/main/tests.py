import datetime
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question

# Create your tests here.
class QuestionModelTest(TestCase):
    def test_was_published_recently_with_future_question(self):
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)
    
    def test_was_published_recently_with_old_question(self):
        time = timezone.now()-datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        time = timezone.now()-datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)
    
def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)

class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('main:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        create_question(question_text='Questão no passado.', days=-30)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado.>']
        )

    def test_future_question(self):
        create_question(question_text="Questão do futuro.", days=30)
        response = self.client.get(reverse('main:index'))
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_question_and_past_question(self):
        create_question(question_text='Questão no passado.', days=-30)
        create_question(question_text="Questão no futuro.", days=30)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado.>']
        )
    
    def test_two_past_questions(self):
        create_question(question_text='Questão no passado 1.', days=-30)
        create_question(question_text='Questão no passado 2.', days=-5)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado 2.>',
            '<Question: Questão no passado 1.>']
        )

class QuestionDetailView(TestCase):
    def test_future_question(self):
        future_question = create_question(
            question_text = "Questão no futuro", days = 5
        )
        url = reverse('main:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(
            question_text = "Questão no passado.", days = -5
        )
        url = reverse('main:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
