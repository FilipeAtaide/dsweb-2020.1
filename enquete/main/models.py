from django.db import models
from django.utils import timezone
import datetime



# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Data de publicação')

    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        now = timezone.now()
        return now-datetime.timedelta(days=1)<=self.pub_date<=now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Publicada recentemente'

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class Comment(models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    pub_date = models.DateTimeField('Data de publicação')
    comment_autor = models.CharField(max_length=200)
    comment_text = models.TextField()

    def __str__(self):
        return self.comment_text
    